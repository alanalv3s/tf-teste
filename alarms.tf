module "cpu_alarm" {
  source = "./modules/alarms"

  alarm_name          = "cpu-utilization"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120" #seconds
  statistic           = "Average"
  threshold           = "80"
  alarm_description   = "EC2 80% cpu utilization"
  dimensions = {
    InstanceId = module.webserver.ec2_web.id
  }
}

module "health_alarm" {
  source = "./modules/alarms"

  alarm_name          = "web-health-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "StatusCheckFailed"
  namespace           = "AWS/EC2"
  period              = "300"
  statistic           = "Average"
  threshold           = "1"
  alarm_description   = "EC2 health status"
  dimensions = {
    InstanceId = module.webserver.ec2_web.id
  }
}

module "disk_alarm" {
  source = "./modules/alarms"

  alarm_name          = "disk_utilization"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "disk_used_percent"
  namespace           = "CWAgent"
  period              = "300"
  statistic           = "Maximum"
  threshold           = "80"
  alarm_description   = "Disk usage is above 80%"
  dimensions = {
    InstanceId = module.webserver.ec2_web.id
    path       = "/"
    device     = "xvda1"
    fstype     = "ext4"
  }
}

module "mem_alarm" {
  source = "./modules/alarms"

  alarm_name          = "mem_utilization"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "mem_used_percent"
  namespace           = "CWAgent"
  period              = "120"
  statistic           = "Average"
  threshold           = "80"
  alarm_description   = "Memory usage is above 80%"
  dimensions = {
    InstanceId = module.webserver.ec2_web.id
  }
}

