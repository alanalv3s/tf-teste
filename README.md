## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

This is all you need to run this project.
* terraform
  ```sh
  curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
  sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
  sudo apt-get update && sudo apt-get install terraform
  ```

* aws-cli
  ```sh
  curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
  unzip awscliv2.zip
  sudo ./aws/install
  ```

* aws-account
https://aws.amazon.com/free/

* aws-credentials
https://console.aws.amazon.com/iam/home?#/security_credentials

* aws-credentials configured
  ```sh
  aws configure --profile tf
  ```

### Installation

1. Clone the repo and go to project folder
   ```sh
   git clone https://gitlab.com/alanalv3s/tf-teste.git
   cd tf-teste
   ```

### Usage

1. Configure variables in vars.tf and run on the root project folder:
  ```sh
  terraform init
  terraform plan -out tf-plan
  terraform apply tf-plan
  ```
2. After the test use the following command to destroy the infrastructure created:
  ```sh
  terraform destroy -auto-approve
  ```
