variable "aws_region" {
  default = "us-east-1"
}

variable "dns_zone" {
  type    = string
  default = "exemplo.com.br"
}

variable "subdomain" {
  type    = string
  default = "teste"
}

variable "email" {
  type    = string
  default = "exemplo@gmail.com"
}
