data "aws_route53_zone" "main" {
  name = var.dns_zone
}

data "aws_elb_hosted_zone_id" "main" {}

resource "aws_route53_record" "main" {
  zone_id         = data.aws_route53_zone.main.zone_id
  name            = "${var.subdomain}.${var.dns_zone}"
  type            = "A"
  allow_overwrite = true
  ttl             = "300"
  records         = [module.webserver.eip]
}
