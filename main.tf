terraform {
  required_version = ">=0.15.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">=3.37.0"
    }

  }
}
provider "aws" {
  profile = "tf"
  region  = var.aws_region
}

locals {
  userdata = templatefile("userdata.sh", {
    ssm_cloudwatch_config = aws_ssm_parameter.cw_agent.name
    site                  = "${var.subdomain}.${var.dns_zone}"
    email                 = var.email
  })
}
module "webserver" {
  source = "./modules/ec2"

  instance_name        = "Alan-Webserver"
  user_data            = local.userdata
  security_groups      = [module.sg.web_sg_id]
  public_key           = file("~/.ssh/id_rsa.pub")
  iam_instance_profile = aws_iam_instance_profile.main.name
}

module "sg" {
  source = "./modules/sg"

  ssh_ips        = ["45.231.155.67/32"]
  sg_name        = "webserver-sg-nginx"
  sg_description = "Allow HTTP/S and SSH to defined IPs"
}

resource "aws_sns_topic_subscription" "email" {
  topic_arn = module.health_alarm.sns_topic
  protocol  = "email"
  endpoint  = var.email
}

resource "aws_ssm_parameter" "cw_agent" {
  description = "Cloudwatch agent config to configure custom log"
  name        = "/cloudwatch-agent/config"
  type        = "String"
  value       = file("cw_agent_config.json")
}
