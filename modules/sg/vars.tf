variable "ssh_ips" {
  type = list(string)
}

variable "sg_name" {
  type = string
}
variable "sg_description" {
  type = string
}
