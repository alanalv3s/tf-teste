variable "alarm_name" {
  type        = string
  description = "The descriptive name for the alarm. This name must be unique within the user's AWS account"
}

variable "comparison_operator" {
  type        = string
  description = "GreaterThanOrEqualToThreshold, GreaterThanThreshold, LessThanThreshold, LessThanOrEqualToThreshold. Additionally, the values LessThanLowerOrGreaterThanUpperThreshold, LessThanLowerThreshold, and GreaterThanUpperThreshold"
}

variable "evaluation_periods" {
  type        = string
  description = "The number of periods over which data is compared to the specified threshold."
  default     = "2"
}

variable "metric_name" {
  type        = string
  description = "The name for the alarm's associated metric EX:CPUUtilization"
}

variable "namespace" {
  type        = string
  description = "The namespace for the alarm's associated metric EX: AWS/EC2"
  default     = "AWS/EC2"
}

variable "period" {
  type        = string
  description = "The period in seconds over which the specified statistic is applied."
  default     = "120"
}

variable "statistic" {
  type        = string
  description = "The statistic to apply to the alarm's associated metric. Either of the following is supported: SampleCount, Average, Sum, Minimum, Maximum"
  default     = "Average"
}

variable "threshold" {
  type        = string
  description = "The value against which the specified statistic is compared"
}

variable "alarm_description" {
  type = string
}
variable "dimensions" {
  description = "The dimensions for the alarm's associated metric."
  type        = any
  default     = null
}
