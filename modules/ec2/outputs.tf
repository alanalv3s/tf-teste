output "ec2_web" {
  value = aws_instance.web
}

output "eip" {
  value = aws_eip.web.public_ip
}
