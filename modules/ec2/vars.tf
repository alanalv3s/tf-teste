variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "ebs_size" {
  type    = number
  default = 8
}

variable "ebs_type" {
  type    = string
  default = "gp3"
}

variable "key_name" {
  type    = string
  default = "default"
}

variable "instance_name" {
  type    = string
  default = "WebServer"
}

variable "user_data" {
  type    = string
  default = ""
}

variable "security_groups" {
  type    = list(string)
  default = []
}

variable "public_key" {
  type = string
}

variable "iam_instance_profile" {
  type    = string
  default = ""
}
