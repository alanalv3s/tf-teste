data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}


resource "aws_instance" "web" {
  ami                  = data.aws_ami.ubuntu.id
  instance_type        = var.instance_type
  key_name             = aws_key_pair.ssh.id
  user_data            = var.user_data
  iam_instance_profile = var.iam_instance_profile
  root_block_device {
    volume_size = var.ebs_size
    volume_type = var.ebs_type
  }
  vpc_security_group_ids = var.security_groups
  tags = {
    Name = var.instance_name
  }

}

resource "aws_eip" "web" {
  instance = aws_instance.web.id
  vpc      = true
}

resource "aws_key_pair" "ssh" {
  key_name   = var.key_name
  public_key = var.public_key
}
