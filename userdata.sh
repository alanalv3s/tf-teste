#!/bin/bash
apt update -y
apt upgrade -y

# Install and configure nginx with HTTPS

apt install nginx certbot python3-certbot-nginx -y
echo "<style>h1, div {text-align: center;}</style><div><img src='https://ton.com.br/checkout/static/media/logo.5b4f2bec.svg'></div><h1>DevOps - Alan Alves de Moura Silva</h1>" > /var/www/html/index.html
systemctl start nginx
systemctl enable nginx
certbot -n --nginx --agree-tos -d ${site} -m ${email} --redirect

# Configure Cloudwatch agent
wget https://s3.amazonaws.com/amazoncloudwatch-agent/ubuntu/amd64/latest/amazon-cloudwatch-agent.deb
dpkg -i -E ./amazon-cloudwatch-agent.deb

# Use cloudwatch config from SSM
/opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl \
-a fetch-config \
-m ec2 \
-c ssm:${ssm_cloudwatch_config} -s